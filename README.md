# Drupal Commerce plugin for Paylike payments

This plugin is *not* developed or maintained by Paylike but kindly made
available by a user.

Released under the GPL V3 license: https://opensource.org/licenses/GPL-3.0

## Supported Drupal Commerce versions

*The plugin has been tested with most versions of Drupal Commerce at every iteration. We recommend using the latest version of Ubercart, but if that is not possible for some reason, test the plugin with your Ubercart version and it would probably function properly.*

* Drupal Commerce
 version last tested on: *8.x-2.15* on Drupal 8.7.10


## Installation

Once you have installed Drupal Commerce on your Drupal setup, follow these simple steps:
  Signup at (paylike.io) [https://paylike.io] (it’s free)

   1. Create a live account
   1. Create an app key for your Drupal website
   1. To install the module via Composer use "composer require drupal/commerce_paylike" command.
   1. Activate the plugin through the 'Extend' screen in Drupal (admin/modules).
   1. Visit Payment gateways configuration page (admin/commerce/config/payment-gateways), 
       add a new payment method, select Paylike plugin, fill the public and private key fields. 
       Configure optional fields if needed.
   1. Select transaction mode for your store on checkout flows page (admin/commerce/config/checkout-flows)
      in payment process pane. Don't forget to click "Save" button after configuring your flow. 
      This module supports immediate (Authorize and capture) or delayed (Authorize only) capture modes. 
      Immediate capture will be done when users confirm their orders. In delayed mode administrator should capture the money manually from
       order administration page (admin/commerce/orders).
       
## Updating settings

Under the Paylike payment method settings, you can:
 * Update the payment method text in the payment gateways list
 * Update the payment method description in the payment gateways list
 * Update the title that shows up in the payment popup 
 * Add public/private keys
 * Set payment mode (test/live)
 
 ## How to
 
 1. Capture
 * In Instant mode, the orders are captured automatically
 * In delayed mode you can capture an order by using the Capture button in the Payments Tab
 2. Refund
   * To refund an order you can use the Refund button (only for captured payments) in the Payments Tab.
 3. Void
   * To void an order you can use the Void button in the Payments Tab. Click the arrow near Capture button then Void.
